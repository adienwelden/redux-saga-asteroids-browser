import axios from 'axios';

import { appConfig } from '../config/constants';

export default function api(token = null, endpoint = '', method = 'GET', body = {}) {

  console.log("the api was called with:")
  console.log("token", token)
  console.log("endpoint", endpoint)
  console.log("method", method) 
  console.log("body", body)

  let response = { ok: false, payload: null, };

  const baseConfig = {
    url: `${appConfig.nasaUrl}?api_key=${appConfig.nasaKey}`,
    method: method,
    responseType: 'json',
    // headers: { Authorization: `Bearer ${token}` },
  };

//   const config = method === 'post' ? Object.assign({}, baseConfig, { data: body }) : baseConfig;
//   const requestAt = formatDateView(new Date().getTime());
  // console.log(config);

  return axios(baseConfig)
    .then(res => {
      // responseAt = formatDateView(new Date().getTime());
      // console.log("==========================================");
      // console.log("================= INICIO =================");
      // console.log("==========================================");
      // console.log("Configuração da api chamada às", requestAt, ":");
      // console.log(config);
    //   console.log("Sucesso na conexão da api:");
    //   console.log(res.data);
      // console.log("==========================================");
      // console.log("=================== FIM ==================");
      // console.log("==========================================");
      response.ok = true;
      response.payload = res.data;
      return response;
    })
    .catch(err => {
    //   console.log("==========================================");
    //   console.log("================= INICIO =================");
    //   console.log("==========================================");
    //   console.log("Configuração da api chamada às", requestAt, ":");
    //   console.log(config);
    //   console.log("==========================================");
      console.log("Erro na conexão da api:");
      console.log(err.response.status, err.response.statusText);
    //   console.log("==========================================");
    //   console.log("=================== FIM ==================");
    //   console.log("==========================================");
      response.ok = false;
      response.payload = err.response.statusText || 'Not Found';
      return response;
    });

}
