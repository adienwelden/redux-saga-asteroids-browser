import * as types from './actionTypes';

export const requestPage = page => ({
    type: types.AST_FETCH_PAGE_REQUESTED,
    payload: { // ignored
        endpoint: '/browse',
        page: page,
    },
});

export const fetchPageSuceeded = payload => ({
    type: types.AST_FETCH_PAGE_SUCCEEDED,
    payload,
})

export const fetchPageFailed = payload => ({
    type: types.AST_FETCH_PAGE_FAILED,
    payload,
})