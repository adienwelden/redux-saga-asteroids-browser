import * as types from '../actions/actionTypes';

const initialState = {
	asteroids: null,
	message: null,
	isLoading: false,
}

export default function (state = initialState, action) {
	switch (action.type) {

		case types.AST_FETCH_PAGE_REQUESTED:
			return { ...state, message: null, isLoading: true, };

		case types.AST_FETCH_PAGE_SUCCEEDED:
			return { ...state, message: null, isLoading: false, asteroids: action.payload, };

		case types.AST_FETCH_PAGE_FAILED:
			return { ...state, message: action.payload, isLoading: false, };

		default:
			return initialState;

	}
}

export const isLoading = state => state.asteroids.isLoading;

export const getMessage = state => state.asteroids.message;

export const getAsteroids = state => state.asteroids.asteroids;

export const getAsteroid = (state, id) => 
	state.asteroids.asteroids.filter(item => 
		item.neo_reference_id === id)[0];