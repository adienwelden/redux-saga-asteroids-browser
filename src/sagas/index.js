import { all } from 'redux-saga/effects';

import {
    astFetchPageWatcher,
} from './asteroids';

export default function* rootSaga() {
    yield all([
        astFetchPageWatcher(),
    ]);
}
