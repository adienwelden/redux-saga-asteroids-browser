import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../utils/api';
import { AST_FETCH_PAGE_REQUESTED } from '../actions/actionTypes';
import * as actions from '../actions/asteroids';

function* fetchPage(action) {
   try {
      const response = yield call(api, action.payload);
      if(response.ok) {
         yield put(actions.fetchPageSuceeded(response.payload.near_earth_objects));
      } else {
         yield put(actions.fetchPageFailed(response.payload));
      }
   } catch (e) {
      yield put(actions.fetchPageFailed(e.message));
   }
}

export function* astFetchPageWatcher() {
  yield takeLatest(AST_FETCH_PAGE_REQUESTED, fetchPage);
}