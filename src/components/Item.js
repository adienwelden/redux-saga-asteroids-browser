import React from 'react';

const Item = props => (
    <div>
        <div>Name: { props.data.name }</div>
        <div>Min length: { props.data.estimated_diameter.kilometers.estimated_diameter_min }</div>
        <div>Max length: { props.data.estimated_diameter.kilometers.estimated_diameter_max }</div>
        <div>------------------------------------------------------------</div>
    </div>
);

export default Item;