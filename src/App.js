import React, { Component } from 'react';
import { connect } from 'react-redux';

import './App.css';
import List from './components/List';

import * as asteroidActions from './actions/asteroids';
import * as asteroidReducers from './reducers/asteroids';

class App extends Component {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(asteroidActions.requestPage(0))
  }

  render() {

    const { isLoading, message, asteroids } = this.props;

    return (
      <div className="App">

        {
          isLoading &&
            <div>loading...</div>
        }

        {
          message &&
            <div>{ `Ocorreu um erro: ${message}` }</div>
        }

        {
          asteroids &&
            <List
              data={asteroids}
            />
        }

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isLoading: asteroidReducers.isLoading(state),
  message: asteroidReducers.getMessage(state),
  asteroids: asteroidReducers.getAsteroids(state),
})

export default connect(mapStateToProps)(App);
